﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace HostEditor
{
    public partial class Main : Form, IMessageFilter
    {
        private readonly Regex reHostEntry =
            new Regex(@"^(?<enabled>#?)(?<ip>[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})[\t| ]*(?<domain>.*)$");

        private readonly Regex reHost = new Regex(@"([a-zA-Z0-9-\.]+\.[a-zA-Z]{2,4})");

        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = HostFilePath;
            LoadHostFile();
        }

        protected void flowLayoutPanel1_Resize(object sender, EventArgs e)
        {
            var ctrl = (FlowLayoutPanel)sender;
            var padding = ctrl.Margin.Left + ctrl.Margin.Right + 20;
            var controls = ctrl.Controls;
            foreach (Control control in controls)
            {
                control.Width = ctrl.Width - padding;
            }
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            LoadHostFile();
        }

        private string HostFilePath
        {
            get
            {
                return Environment.SystemDirectory + @"\drivers\etc\hosts";
            }
        }

        private int ControlWidth
        {
            get { return flowLayoutPanel1.Width - (20 + flowLayoutPanel1.Margin.Left + flowLayoutPanel1.Margin.Right); }
        }

        private void LoadHostFile()
        {
            flowLayoutPanel1.SuspendLayout();
            flowLayoutPanel1.Controls.Clear();

            try
            {
                using (var stream = new StreamReader(HostFilePath))
                {
                    string input;
                    while ((input = stream.ReadLine()) != null)
                    {
                        if (!reHostEntry.IsMatch(input)) continue;

                        var match = reHostEntry.Match(input);

                        var ip = match.Groups["ip"].Value;
                        var hosts = match.Groups["domain"].Value;
                        if (!reHost.IsMatch(hosts)) return;

                        var g = reHost.Matches(hosts);
                        foreach (Match m in g)
                        {
                            var enabled = string.IsNullOrEmpty(match.Groups["enabled"].Value);
                            flowLayoutPanel1.Controls.Add(new HostEntryControl
                                                              {
                                                                  IPAddress = ip,
                                                                  Host = m.Value,
                                                                  Width = ControlWidth,
                                                                  Enabled = enabled
                                                              });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                flowLayoutPanel1.ResumeLayout();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var sb = new StringBuilder();
            using (var stream = new StreamReader(HostFilePath))
            {
                string input;
                while ((input = stream.ReadLine()) != null)
                {
                    if (reHostEntry.IsMatch(input)) continue;
                    sb.AppendLine(input);
                }
            }
            foreach (HostEntryControl control in flowLayoutPanel1.Controls)
            {
                sb.AppendLine(control.GetLineData());
            }
            try
            {
                using (var stream = new StreamWriter(HostFilePath, false))
                {
                    // write the data back to the file
                    stream.Write(sb.ToString());
                    stream.Flush();
                }
            }
            catch (UnauthorizedAccessException)
            {
                // need elevated permissions to edit the host file
                MessageBox.Show(
                    @"You need elevated permissions to edit the host file. 
If you are running Vista or Windows 7, right click and select 'Run as administrator'.  Windows XP users make sure
the file is not marked 'Read only'.",
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                // show error
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void btnAddEntry_Click(object sender, EventArgs e)
        {
            var ctrl = new HostEntryControl { Width = ControlWidth };
            flowLayoutPanel1.Controls.Add(ctrl);
            ctrl.Focus();
        }

        private void Main_KeyDown(object sender, KeyEventArgs e)
        {
            if (!(e.Control && e.KeyCode == Keys.V)) return;

            // check the clipboard for data
            var data = Clipboard.GetText();
            if (string.IsNullOrEmpty(data)) return;

            AddHostEntry(data);
        }

        private void AddHostEntry(string data)
        {
            var ctrl = new HostEntryControl { IPAddress = data, Width = ControlWidth };
            flowLayoutPanel1.Controls.Add(ctrl);
            ctrl.Focus();
        }

        public bool PreFilterMessage(ref Message m)
        {
            return false;// no filtering now
        }
    }
}