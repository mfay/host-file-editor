﻿using System;
using System.Windows.Forms;

namespace HostEditor
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var main = new Main();
            Application.AddMessageFilter(main);
            Application.Run(main);
        }
    }
}