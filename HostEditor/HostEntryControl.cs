﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using HostEditor.Properties;

namespace HostEditor
{
    public partial class HostEntryControl : UserControl
    {
        public HostEntryControl()
        {
            InitializeComponent();
        }

        public String IPAddress
        {
            get { return textBox1.Text; }
            set { textBox1.Text = value; }
        }

        public String Host
        {
            get { return textBox2.Text; }
            set { textBox2.Text = value; }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            Parent.Controls.Remove(this);
        }

        public string GetLineData()
        {
            return string.Format("{0}{1}\t{2}", (Enabled) ? "" : "#" ,IPAddress, Host);
        }

        private void btnDisable_Click(object sender, EventArgs e)
        {
            Enabled = !Enabled;
        }

        public new bool Enabled
        {
            get { return textBox1.Enabled; }
            set
            {
                textBox1.Enabled = textBox2.Enabled = value;
                btnDisable.Image = (value)
                                       ? Resources.button_unlocked
                                       : Resources.button_locked;
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            Process.Start("http://" + textBox2.Text);
        }
    }
}